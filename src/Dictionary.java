import java.util.Map;

public class Dictionary {

    private Map<String, String> dicitionaryMap;

    public Dictionary(Map<String, String> incomingMap) {
        this.dicitionaryMap = incomingMap;
    }

    public void addToDictionary(String polskieSlowo, String angielskieSlowo) {
        dicitionaryMap.put(polskieSlowo, angielskieSlowo);
    }

    public boolean editDictionaryWord(String polskieSlowo, String angielskieSlowo) {
        if (dicitionaryMap.get(polskieSlowo) == null) {
            dicitionaryMap.put(polskieSlowo, angielskieSlowo);
            return false;
        } else {
            dicitionaryMap.put(polskieSlowo, angielskieSlowo);
            return true;
        }

    }
}