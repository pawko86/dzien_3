import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

     Map<String, String> dictionary = new HashMap<String, String>();

     ((HashMap) dictionary).put("Zaba", "Frog");
     ((HashMap) dictionary).put("Pies", "Dog");
     ((HashMap) dictionary).put("Kot", "Cat");
     ((HashMap) dictionary).put("Kon", "Horse");
     ((HashMap) dictionary).put("Ptak", "Bird");

        System.out.println(dictionary.get("Zaba"));


        for (String key : dictionary.keySet()){
            System.out.println(key + " " + dictionary.get(key));
        }


    }

}
